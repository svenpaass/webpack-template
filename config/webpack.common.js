const path = require('path');
const webpack = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');
const ExtractTextWebPackPlugin = require('extract-text-webpack-plugin');

module.exports = (entry, prefix) => {
    return {
        entry : entry,
        resolve: {
            // Automatisches erkennen von bestimmten Dateitypen und Verzeichnisnamen bei den Importen
            extensions: [ '.js', '.jsx', '.ts', '.tsx', '.json', '.scss', '.css', '.jpeg', '.jpg', 'png', '.gif', '.svg' ], // Automatically resolve certain extensions
            alias : {
                images: path.resolve(__dirname, '../src/assets/images'),
                icons: path.resolve(__dirname, '../src/assets/icons'),
                stylesheets: path.resolve(__dirname, '../src/assets/stylesheets'),
                media: path.resolve(__dirname, '../src/assets/media')
            }
        },
        module: {
            rules: [
                // JavaScript Dateien
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    loader: "babel-loader"
                },

                // Typescript Support
                {
                    test: /\.tsx?$/,
                    exclude: /node_modules/,
                    use: [
                        { 
                            loader : "babel-loader"
                        },
                        {
                            loader : "ts-loader"
                        }
                    ]
                },

	            // Cascading Style Sheets
	            {
	                test: /\.(css|scss|sass)$/,
	                use: ['css-hot-loader'].concat(ExtractTextWebPackPlugin.extract({  // HMR for styles
                        use: [
	                        { 
	                            loader : 'css-loader',
	                            options : {
	                                sourceMap: true,
	                                importLoaders: 2
	                                // 0 => no loaders (default); 1 => postcss-loader; 2 => postcss-loader, sass-loader
	                            }
                            },
                            {
	                            loader : 'postcss-loader',
	                                options: { 
	                                    config: {
	                                        path: path.resolve(__dirname, '../config/postcss.config.js')
	                                    },
	                                    sourceMap: true
	                                }
	                        },                    
	                        { 
	                            loader : 'sass-loader',
	                            options : {
	                                sourceMap: true
	                            }
                            }
	                    ],
	                    fallback: 'style-loader'                            
	                }))
	            },

                // Bilder
                {
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 8192, // Convert images < 8kb to base64 strings
                                context : 'src/assets/images',
                                name : 'images/' + prefix + '[name].[ext]'
                            }
                        },
                        { 
                            loader : 'image-webpack-loader',
                            options: {
                                mozjpeg: {
                                    progressive: true
                                },
                                gifsicle: {
                                    interlaced: false,
                                },
                                optipng: {
                                    optimizationLevel: 4,
                                },
                                pngquant: {
                                    quality: '75-90',
                                    speed: 3
                                },
                                // Specifying webp here will create a WEBP version of your JPG/PNG images
                                webp: {
                                    quality: 75
                                }
                              }
                        }                        
                    ]
                },

                // Schriftarten 
                //
                // Hinweis: Es wird davon ausgegangen, das sie sich in einem 
                // Verzeichnis mit dem Namen fonts befinde, da sonst font-svg Dateien 
                // in images/ (siehe Bilder) abgelegt würden.
                {
                    test: /fonts\/.*\.(svg|eot|ttf|woff|woff2)$/i,
                    use: [
                        {
                            loader : 'file-loader',
                            options : {
                                name : 'fonts/' + prefix + '[name].[ext]'
                            }
                        }
                    ]
                },

                // Icons
                {
                    test: /\.(ico|icns)$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader : 'file-loader',
                            options : {
                                name : '[name].[ext]'
                            }
                        }
                    ]
                },

                // sonstige Dateien im Verzeichnis media
                {
                    test: /\.(txt|raw|bin|json)$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader : 'file-loader',
                            options : {
                                name : 'media/' + prefix + '[name].[ext]'
                            }
                        }
                    ]
                }            
            ],
        },
        plugins : [
            new HtmlPlugin({
                template: path.resolve(__dirname, '../src/assets/index.template.html')
            }),
            new webpack.ProvidePlugin({     // insert jquery in globals, if it is imported
                $: "jquery",
                jQuery: "jquery"
            })
        ],
        devServer: {
            contentBase : path.resolve(__dirname, '../dist'), // A directory or URL to serve HTML content from.
            historyApiFallback: true, // fallback to /index.html for Single Page Applications.
            inline: true, // inline mode (set to false to disable including client scripts (like livereload)
            open: true, // open default browser while launching
            compress: true, // Enable gzip compression for everything served:
            hot: true // Enable webpack's Hot Module Replacement feature        
        }
    }
}