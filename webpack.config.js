// get environment variables from webpack command line
module.exports = env => {
    const path = require('path');
    const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
    const HtmlPlugin = require('html-webpack-plugin');    
    const ExtractTextWebPackPlugin = require('extract-text-webpack-plugin');
    const DashboardPlugin = require('webpack-dashboard/plugin');  

    // application entry point
    const entryName = './src/index.js';
    const isProductionBuild = (env !== undefined && env.NODE_ENV === 'production') ? true : false;

    const common = require('./config/webpack.common');
    let webpackConfig;

    // prepare configuration (dev or prod)
    if (isProductionBuild) {
        webpackConfig = common(entryName, '[hash]-');

        webpackConfig.output =  {
            path : path.resolve(__dirname, 'dist/prod'),
            filename : '[hash]-bundle.js'
        };

        webpackConfig.plugins.push(
            new ExtractTextWebPackPlugin({ filename: '[hash]-styles.css', disable: false, allChunks: true }),            
            new UglifyJsPlugin({ sourceMap: true })
        );

        webpackConfig.devtool = 'source-map';
    } else {
        webpackConfig = common(entryName, '');

        webpackConfig.output =  {
            path : path.resolve(__dirname, 'dist/dev'),
            filename : 'bundle.js'
        };

        webpackConfig.plugins.push(
            new DashboardPlugin(),
            new ExtractTextWebPackPlugin({ filename: 'styles.css', disable: false, allChunks: true })
        );

        webpackConfig.devtool = 'inline-source-map';
    }

    // use external version from cdn (index.template.html)
    webpackConfig.externals =  {
        "react": "React",
        "react-dom": "ReactDOM",
        "jquery" : "jquery",
        "jquery" : "$"
    }

    // console.log("Webpack-Config: " + JSON.stringify(webpackConfig, null, 4));
    return webpackConfig;
}